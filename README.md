# vagrant-ubuntu64-docker

# Usage

```
$ vagrant up
$ vagrant package --output myBase.box
$ vagrant box add ubuntu16-docker myBase.box
```

To use ubuntu16-docker, we need `private_key` to connect ssh.
`private_key` path is `.vagrant/machines/default/virtualbox/private_key`

## Reference
https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one
http://stackoverflow.com/questions/20081116/vagrant-no-space-left-on-device-error
